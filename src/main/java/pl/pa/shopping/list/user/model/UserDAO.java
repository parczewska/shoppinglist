package pl.pa.shopping.list.user.model;

import javax.persistence.*;
import pl.pa.shopping.list.list.model.ProductsListDTO;

import java.util.List;

@Entity
@Table(name = "user")
public class UserDAO {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "login")
    private String login;

    @Column(name = "user_name")
    private String name;
    @Column(name = "user_lastname")
    private String lastname;
    @Column(name = "products_list")
    private List<ProductsListDTO> lists;

    UserDAO() {
    }

    public UserDAO(String login, String name, String lastname, List<ProductsListDTO> lists) {
        this.login = login;
        this.name = name;
        this.lastname = lastname;
        this.lists = lists;
    }

    public String getLogin() {
        return login;
    }

    String getName() {
        return name;
    }

    String getLastname() {
        return lastname;
    }

    List<ProductsListDTO> getLists() {
        return lists;
    }
}
