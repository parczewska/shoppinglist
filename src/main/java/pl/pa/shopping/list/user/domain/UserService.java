package pl.pa.shopping.list.user.domain;

import org.springframework.stereotype.Service;
import pl.pa.shopping.list.user.error.InvalidUserDataException;
import pl.pa.shopping.list.user.model.UserDAO;
import pl.pa.shopping.list.user.model.UserDTO;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO addUser(UserDTO userDTO) {

        Optional<UserDAO> result = userRepository.findByLogin(userDTO.getLogin());

        if (result.isEmpty()) {

            UserDAO userDAO = new UserDAO(userDTO.getLogin(), userDTO.getName(), userDTO.getLastname(), userDTO.getLists());
            userRepository.save(userDAO);
            return userDTO;
        }
        throw new InvalidUserDataException("Istnieje użytkownik o podanym loginie.");
    }

    public void deleteUser(String login) {

        Optional<UserDAO> result = userRepository.findByLogin(login);

        if (result.isPresent()) {
            userRepository.delete(result.get());
        } else throw new InvalidUserDataException("Nie istnieje użytkownik o podanym loginie.");
    }
}
