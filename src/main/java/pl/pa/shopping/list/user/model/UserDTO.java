package pl.pa.shopping.list.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import pl.pa.shopping.list.list.model.ProductsListDTO;

import java.util.List;

@Builder
public class UserDTO {

    @JsonProperty
    private String login;
    @JsonProperty
    private String name;

    @JsonProperty
    private String lastname;

    @JsonProperty
    private List<ProductsListDTO> lists;

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public List<ProductsListDTO> getLists() {
        return lists;
    }
}
