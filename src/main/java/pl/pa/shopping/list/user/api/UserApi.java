package pl.pa.shopping.list.user.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.shopping.list.user.domain.UserService;
import pl.pa.shopping.list.user.model.UserDTO;

@RestController
class UserApi {

    private UserService userService;

    UserApi(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserDTO addUser(@RequestBody UserDTO userDTO) {
        return userService.addUser(userDTO);
    }

    @RequestMapping(value = "/users/{login}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable String login) {
        userService.deleteUser(login);
    }


}
