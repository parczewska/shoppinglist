package pl.pa.shopping.list.user.model;

public class UserMapper {

    public static UserDTO map(UserDAO userDAO) {

        return UserDTO.builder().login(userDAO.getLogin()).
                name(userDAO.getName()).
                lastname(userDAO.getLastname()).
                lists(userDAO.getLists()).build();
    }

    public static UserDAO map(UserDTO userDTO) {

        return new UserDAO(userDTO.getLogin(), userDTO.getName(),
                userDTO.getLastname(),
                userDTO.getLists());
    }
}
