package pl.pa.shopping.list.product.model;

public enum ProductGroup {

    DAIRY("nabiał"),
    EGGS("jaja"),
    MEAT("mięso"),
    FATS("tłuszcze"),
    CEREAL_PRODUCTS("produkty zbożowe"),
    VEGETABLES("warzywa"),
    FRUITS("owoce"),
    SWEETS("słodycze"),
    NUTS_AND_SEEDS("orzechy i nasiona");

    private final String description;

    private ProductGroup(String description) {
        this.description = description;
    }
}
