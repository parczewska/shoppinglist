package pl.pa.shopping.list.product.domain;

import org.springframework.stereotype.Service;
import pl.pa.shopping.list.product.error.InvalidProductDataException;
import pl.pa.shopping.list.product.model.*;

import java.util.Optional;

@Service
public class ProductBaseService {

    private ProductBaseRepository productBaseRepository;

    public ProductBaseService(ProductBaseRepository productBaseRepository) {
        this.productBaseRepository = productBaseRepository;
    }

    public ProductBaseDTO addProduct(ProductBaseDTO productBaseDTO) {

        Optional<ProductBaseDAO> result = productBaseRepository.findByName(productBaseDTO.getName());

        if (result.isEmpty()) {
            ProductBaseDAO productBaseDAO = ProductBaseMapper.map(productBaseDTO);
            productBaseRepository.save(productBaseDAO);
            return productBaseDTO;
        }
        throw new InvalidProductDataException("Produkt istnieje w bazie.");
    }

    public ProductBaseDTO findProduct(String name) {

        Optional<ProductBaseDAO> result = productBaseRepository.findByName(name);

        if (result.isPresent()) {
            return ProductBaseMapper.map(result.get());
        }
        throw new InvalidProductDataException("Brak podanego produktu.");
    }

    public void deleteProduct(String name) {

        Optional<ProductBaseDAO> result = productBaseRepository.findByName(name);

        if (result.isPresent()) {
            productBaseRepository.delete(result.get());
        } else throw new InvalidProductDataException("Brak podanego produktu.");
    }
}
