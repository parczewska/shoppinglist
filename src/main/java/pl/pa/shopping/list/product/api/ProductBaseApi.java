package pl.pa.shopping.list.product.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.shopping.list.product.domain.ProductBaseService;
import pl.pa.shopping.list.product.model.ProductBaseDTO;
import pl.pa.shopping.list.product.model.ProductDTO;

@RestController
public class ProductBaseApi {

    private ProductBaseService productBaseService;

    public ProductBaseApi(ProductBaseService productBaseService) {
        this.productBaseService = productBaseService;
    }

    @RequestMapping(value = "/products", method = RequestMethod.POST)
    public ProductBaseDTO addProduct(@RequestBody ProductBaseDTO productBaseDTO) {
        return productBaseService.addProduct(productBaseDTO);
    }

    @RequestMapping(value = "/products/{name}", method = RequestMethod.POST)
    public ProductBaseDTO findProduct(@PathVariable String name) {
        return productBaseService.findProduct(name);
    }

    @RequestMapping(value = "/products/{name}", method = RequestMethod.DELETE)
    public void deleteProduct(@PathVariable String name) {
        productBaseService.deleteProduct(name);
    }
}
