package pl.pa.shopping.list.product.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class ProductBaseDTO {
    @JsonProperty
    private String name;
    @JsonProperty
    private ProductGroup productGroup;
    @JsonProperty
    private double calories;

    public String getName() {
        return name;
    }

    public ProductGroup getProductGroup() {
        return productGroup;
    }

    public double getCalories() {
        return calories;
    }
}
