package pl.pa.shopping.list.product.model;


import javax.persistence.*;

@Table(name = "products_base")
@Entity
public class ProductBaseDAO {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "product_group")
    @Enumerated(EnumType.STRING)
    private ProductGroup productGroup;

    @Column(name = "calories")
    private double calories;

    ProductBaseDAO(String name, ProductGroup productGroup, double calories) {
        this.name = name;
        this.productGroup = productGroup;
        this.calories = calories;
    }

    public String getName() {
        return name;
    }

    public ProductGroup getProductGroup() {
        return productGroup;
    }

    double getCalories() {
        return calories;
    }
}
