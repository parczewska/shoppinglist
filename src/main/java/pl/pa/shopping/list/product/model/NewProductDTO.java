package pl.pa.shopping.list.product.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class NewProductDTO {

    @JsonProperty
    private String listName;

    @JsonProperty
    private ProductDTO product;

    public String getListName() {
        return listName;
    }

    public ProductDTO getProduct() {
        return product;
    }

}
