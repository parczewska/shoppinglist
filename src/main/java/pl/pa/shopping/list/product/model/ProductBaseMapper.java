package pl.pa.shopping.list.product.model;

public class ProductBaseMapper {

    private ProductBaseMapper() {
    }

    public static ProductBaseDTO map(ProductBaseDAO productBaseDAO) {

        return ProductBaseDTO.builder().name(productBaseDAO.getName()).
                productGroup(productBaseDAO.getProductGroup()).
                calories(productBaseDAO.getCalories()).build();
    }

    public static ProductBaseDAO map(ProductBaseDTO productBaseDTO) {

        return new ProductBaseDAO(productBaseDTO.getName(),
                productBaseDTO.getProductGroup(),
                productBaseDTO.getCalories());
    }
}
