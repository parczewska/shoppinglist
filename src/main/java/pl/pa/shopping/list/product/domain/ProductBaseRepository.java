package pl.pa.shopping.list.product.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pa.shopping.list.product.model.ProductBaseDAO;

import java.util.Optional;

@Repository
public interface ProductBaseRepository extends JpaRepository<ProductBaseDAO, Long> {

    Optional<ProductBaseDAO> findByName(String name);
}
