package pl.pa.shopping.list.product.model;

public class ProductMapper {

    private ProductMapper() {
    }

    public static ProductDTO map(ProductDAO productDAO) {

        return ProductDTO.builder().name(productDAO.getName()).
                productAmount(productDAO.getProductAmount()).build();
    }

    public static ProductDAO map(ProductDTO productDTO) {

        return new ProductDAO(productDTO.getName(),
                productDTO.getProductAmount());
    }

    public static ProductDAO map(NewProductDTO newProductDTO) {

        return map(newProductDTO.getProduct());

    }
}
