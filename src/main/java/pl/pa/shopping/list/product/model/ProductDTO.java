package pl.pa.shopping.list.product.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Builder;

@Builder
public class ProductDTO {
    @JsonProperty
    private String name;
    @JsonProperty
    private double productAmount;


    public String getName() {
        return name;
    }

    public double getProductAmount() {
        return productAmount;
    }
}
