package pl.pa.shopping.list.product.model;

import javax.persistence.*;

import org.springframework.data.annotation.Id;

import java.util.Objects;

@Table(name = "product")
@Entity
public class ProductDAO {

    @Column(name = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "product_name")
    private String name;
    @Column(name = "product_amount")
    private double productAmount;

    ProductDAO() {
    }

    public ProductDAO(String name, double productAmount) {
        this.name = name;
        this.productAmount = productAmount;
    }

    public String getName() {
        return name;
    }

    public double getProductAmount() {
        return productAmount;
    }

    public void setProductAmount(double productAmount) {
        this.productAmount = productAmount;
    }
}
