package pl.pa.shopping.list.list.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pa.shopping.list.list.model.ProductsListDAO;


import java.util.Optional;

@Repository
public interface ProductsListRepository extends JpaRepository<ProductsListDAO, Long> {

    Optional<ProductsListDAO> findByName(String name);
}
