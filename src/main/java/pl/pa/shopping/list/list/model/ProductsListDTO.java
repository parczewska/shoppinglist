package pl.pa.shopping.list.list.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import pl.pa.shopping.list.product.model.ProductDTO;

import java.util.List;
import java.util.Set;

@Builder
public class ProductsListDTO {

    @JsonProperty
    private String name;

    @JsonProperty
    private List<ProductDTO> products;

    public String getName() {
        return name;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

}
