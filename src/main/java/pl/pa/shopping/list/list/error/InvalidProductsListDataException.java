package pl.pa.shopping.list.list.error;

public class InvalidProductsListDataException extends RuntimeException {

    public InvalidProductsListDataException(String message) {
        super(message);
    }
}
