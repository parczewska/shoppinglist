package pl.pa.shopping.list.list.model;

import pl.pa.shopping.list.product.model.ProductDAO;
import pl.pa.shopping.list.product.model.ProductDTO;
import pl.pa.shopping.list.product.model.ProductBaseMapper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductListMapper {

    public static ProductsListDAO map(ProductsListDTO productsListDTO) {

        return new ProductsListDAO(productsListDTO.getName(), map(productsListDTO.getProducts()));
    }

    public static ProductsListDTO map(ProductsListDAO productsListDAO) {

        return ProductsListDTO.builder().name(productsListDAO.getName()).
                products(mapToDTOs(productsListDAO.getProducts())).build();
    }

    private static List<ProductDTO> mapToDTOs(Collection<ProductDAO> productsDAO) {

        List<ProductDTO> products = new ArrayList<>();

        productsDAO.forEach(e -> products.add(map(e)));
        return products;
    }

    private static ProductDTO map(ProductDAO productDAO) {

        return ProductDTO.builder().name(productDAO.getName()).
                productAmount(productDAO.getProductAmount()).build();
    }

    private static List<ProductDAO> map(Collection<ProductDTO> productsDTO) {

        List<ProductDAO> products = new ArrayList<>();

        productsDTO.forEach(e -> products.add(map(e)));
        return products;
    }

    private static ProductDAO map(ProductDTO productDTO) {

        return new ProductDAO(productDTO.getName(),
                productDTO.getProductAmount());
    }
}
