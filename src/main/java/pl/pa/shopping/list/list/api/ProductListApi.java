package pl.pa.shopping.list.list.api;

import org.springframework.web.bind.annotation.*;
import pl.pa.shopping.list.list.domain.ProductsListService;
import pl.pa.shopping.list.list.model.ProductsListDTO;
import pl.pa.shopping.list.product.model.NewProductDTO;
import pl.pa.shopping.list.product.model.ProductDTO;

@RestController
public class ProductListApi {

    private ProductsListService productsListService;

    ProductListApi(ProductsListService productsListService) {
        this.productsListService = productsListService;
    }

    @RequestMapping(value = "/productsList", method = RequestMethod.POST)
    public ProductsListDTO addProductsList(@RequestBody ProductsListDTO productsListDTO) {
        return productsListService.addProductsList(productsListDTO);
    }

    @RequestMapping(value = "/productList/newProduct", method = RequestMethod.POST)
    public ProductsListDTO addProductToList(@RequestBody NewProductDTO newProductDTO) {
        return productsListService.addProductToList(newProductDTO);
    }

    @RequestMapping(value = "/productsList/{name}", method = RequestMethod.DELETE)
    public void deleteProductsList(@PathVariable String name) {
        productsListService.deleteProductsList(name);
    }

    @RequestMapping(value = "/productList/newProduct", method = RequestMethod.DELETE)
    public void deleteProductFromList(@PathVariable String name, @RequestBody ProductDTO productDTO) {
        productsListService.deleteProductFromList(name, productDTO);
    }

}
