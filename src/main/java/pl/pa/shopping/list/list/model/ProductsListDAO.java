package pl.pa.shopping.list.list.model;

import org.springframework.data.annotation.Id;
import pl.pa.shopping.list.product.model.ProductDAO;
import pl.pa.shopping.list.product.model.ProductDTO;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "products_list")
public class ProductsListDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "list_name")
    private String name;
    @Column(name = "products")
    private List<ProductDAO> products;

    ProductsListDAO() {
    }

    public ProductsListDAO(String name, List<ProductDAO> products) {
        this.name = name;
        this.products = products;
    }

    public String getName() {
        return name;
    }

    public List<ProductDAO> getProducts() {
        return products;
    }

    public void addProduct(ProductDAO productDAO) {
        products.add(productDAO);
    }
}
