package pl.pa.shopping.list.list.domain;

import org.springframework.stereotype.Service;
import pl.pa.shopping.list.list.error.InvalidProductsListDataException;
import pl.pa.shopping.list.list.model.ProductListMapper;
import pl.pa.shopping.list.list.model.ProductsListDAO;
import pl.pa.shopping.list.list.model.ProductsListDTO;
import pl.pa.shopping.list.product.domain.ProductBaseRepository;
import pl.pa.shopping.list.product.error.InvalidProductDataException;
import pl.pa.shopping.list.product.model.*;


import java.util.List;
import java.util.Optional;


@Service
public class ProductsListService {

    private ProductsListRepository productsListRepository;
    private ProductBaseRepository productBaseRepository;

    public ProductsListService(ProductsListRepository productsListRepository, ProductBaseRepository productBaseRepository) {
        this.productsListRepository = productsListRepository;
        this.productBaseRepository = productBaseRepository;
    }

    public ProductsListDTO addProductsList(ProductsListDTO productsListDTO) {

        Optional<ProductsListDAO> result = productsListRepository.findByName(productsListDTO.getName());

        if (result.isEmpty()) {
            ProductsListDAO productsListDAO = ProductListMapper.map(productsListDTO);
            productsListRepository.save(productsListDAO);
            return productsListDTO;
        }
        throw new InvalidProductsListDataException("Istnieje lista zakupów o podanej nazwie.");
    }

    public ProductsListDTO addProductToList(NewProductDTO newProductDTO) {

        Optional<ProductsListDAO> result = productsListRepository.findByName(newProductDTO.getListName());

        if (result.isPresent()) {

            ProductDAO productDAO = ProductMapper.map(newProductDTO);
            ProductsListDAO productsListDAO = result.get();
            productsListDAO.addProduct(productDAO);
            productsListRepository.save(productsListDAO);

            return ProductListMapper.map(productsListDAO);
        }
        throw new InvalidProductsListDataException("Nie istnieje lista zakupów o podanej nazwie.");
    }

    public void deleteProductsList(String name) {

        Optional<ProductsListDAO> result = productsListRepository.findByName(name);

        if (result.isPresent()) {
            productsListRepository.delete(result.get());
        } else throw new InvalidProductsListDataException("Brak listy zakupów o podanej nazwie.");
    }

    public void deleteProductFromList(String name, ProductDTO productDTO) {

        Optional<ProductsListDAO> result = productsListRepository.findByName(name);

        if (result.isPresent()) {

            ProductsListDAO productsListDAO = result.get();
            List<ProductDAO> products = productsListDAO.getProducts();

            Optional<ProductDAO> productResult = products.stream().filter(e -> e.getName().equals(productDTO.getName())).findFirst();

            if (productResult.isPresent()) {

                ProductDAO productDAO = productResult.get();
                double amount = productDAO.getProductAmount();

                if (amount - productDTO.getProductAmount() <= 0) {
                    products.remove(productDAO);

                } else productDAO.setProductAmount(amount - productDTO.getProductAmount());
                productsListRepository.save(productsListDAO);

            } else throw new InvalidProductDataException("Na liście nie ma produktu o podanej nazwie.");
        } else throw new InvalidProductsListDataException("Nie istnieje lista o podanej nazwie.");
    }
}
