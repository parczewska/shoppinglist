package pl.pa.shopping.list.list.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import pl.pa.shopping.list.list.model.ProductsListDAO;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

public class ProductsListRepositoryInMemory implements ProductsListRepository {

    private Set<ProductsListDAO> productslists = new HashSet<>();

    @Override
    public Optional<ProductsListDAO> findByName(String name) {
        return productslists.stream().filter(e -> e.getName().equals(name)).findFirst();
    }

    @Override
    public List<ProductsListDAO> findAll() {
        return null;
    }

    @Override
    public List<ProductsListDAO> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<ProductsListDAO> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<ProductsListDAO> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(ProductsListDAO entity) {
        productslists.remove(entity);

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends ProductsListDAO> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends ProductsListDAO> S save(S entity) {
        productslists.add(entity);
        return entity;
    }

    @Override
    public <S extends ProductsListDAO> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<ProductsListDAO> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends ProductsListDAO> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends ProductsListDAO> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<ProductsListDAO> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> longs) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public ProductsListDAO getOne(Long aLong) {
        return null;
    }

    @Override
    public ProductsListDAO getById(Long aLong) {
        return null;
    }

    @Override
    public ProductsListDAO getReferenceById(Long aLong) {
        return null;
    }

    @Override
    public <S extends ProductsListDAO> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends ProductsListDAO> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends ProductsListDAO> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends ProductsListDAO> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ProductsListDAO> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends ProductsListDAO> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends ProductsListDAO, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
