package pl.pa.shopping.list.list.api;

import org.junit.jupiter.api.BeforeAll;
import pl.pa.shopping.list.list.domain.ProductsListRepository;
import pl.pa.shopping.list.list.domain.ProductsListRepositoryInMemory;
import pl.pa.shopping.list.list.domain.ProductsListService;
import pl.pa.shopping.list.list.model.ProductsListDTO;
import pl.pa.shopping.list.product.api.ProductBaseApi;
import pl.pa.shopping.list.product.domain.ProductBaseRepository;
import pl.pa.shopping.list.product.domain.ProductBaseRepositoryInMemory;
import pl.pa.shopping.list.product.domain.ProductBaseService;
import pl.pa.shopping.list.product.model.NewProductDTO;
import pl.pa.shopping.list.product.model.ProductBaseDTO;
import pl.pa.shopping.list.product.model.ProductDTO;
import pl.pa.shopping.list.product.model.ProductGroup;

public class ProductListApiTest {

    private static ProductBaseRepository productBaseRepository = new ProductBaseRepositoryInMemory();
    private static ProductBaseService productBaseService = new ProductBaseService(productBaseRepository);
    private static ProductBaseApi productBaseApi = new ProductBaseApi(productBaseService);
    private static ProductsListRepository productsListRepository = new ProductsListRepositoryInMemory();
    private static ProductsListService productsListService = new ProductsListService(productsListRepository, productBaseRepository);
    private static ProductListApi productListApi = new ProductListApi(productsListService);

    @BeforeAll
    static void initProducts() {
        ProductBaseDTO product1 = ProductBaseDTO.builder().name("mleko").
                productGroup(ProductGroup.DAIRY).
                calories(70.0).build();
        productBaseApi.addProduct(product1);

        ProductBaseDTO product2 = ProductBaseDTO.builder().name("ryż").
                productGroup(ProductGroup.CEREAL_PRODUCTS).
                calories(150.0).build();
        productBaseApi.addProduct(product2);

        ProductBaseDTO product3 = ProductBaseDTO.builder().name("papryka").
                productGroup(ProductGroup.VEGETABLES).
                calories(10.0).build();
        productBaseApi.addProduct(product3);

        ProductBaseDTO product4 = ProductBaseDTO.builder().name("cytryna").
                productGroup(ProductGroup.FRUITS).
                calories(5.0).build();
        productBaseApi.addProduct(product4);
    }

    @BeforeAll
    static void initProductsList() {

//        NewProductDTO newProductDTO = NewProductDTO.builder().listName("śniadanie").product(ProductDTO.builder().name("mleko")).build();
//
//        ProductsListDTO list1 = ProductsListDTO.builder().name("śniadanie").products(productListApi.addProductToList(p))
    }

}
