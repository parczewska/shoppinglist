package pl.pa.shopping.list.product.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import pl.pa.shopping.list.product.model.ProductBaseDAO;


import java.util.*;
import java.util.function.Function;

public class ProductBaseRepositoryInMemory implements ProductBaseRepository {

    private Set<ProductBaseDAO> products = new HashSet<>();

    @Override
    public Optional<ProductBaseDAO> findByName(String name) {
        return products.stream().filter(e -> e.getName().equals(name)).findFirst();
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends ProductBaseDAO> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends ProductBaseDAO> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<ProductBaseDAO> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> longs) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public ProductBaseDAO getOne(Long aLong) {
        return null;
    }

    @Override
    public ProductBaseDAO getById(Long aLong) {
        return null;
    }

    @Override
    public ProductBaseDAO getReferenceById(Long aLong) {
        return null;
    }

    @Override
    public <S extends ProductBaseDAO> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends ProductBaseDAO> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends ProductBaseDAO> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends ProductBaseDAO> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends ProductBaseDAO> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends ProductBaseDAO> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends ProductBaseDAO, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }

    @Override
    public <S extends ProductBaseDAO> S save(S entity) {
        products.add(entity);
        return entity;

    }

    @Override
    public <S extends ProductBaseDAO> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<ProductBaseDAO> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public List<ProductBaseDAO> findAll() {
        return null;
    }

    @Override
    public List<ProductBaseDAO> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(ProductBaseDAO entity) {
        products.remove(entity);

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends ProductBaseDAO> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public List<ProductBaseDAO> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<ProductBaseDAO> findAll(Pageable pageable) {
        return null;
    }
}
