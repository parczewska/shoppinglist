package pl.pa.shopping.list.product.api;

import org.junit.jupiter.api.*;
import pl.pa.shopping.list.product.domain.ProductBaseRepository;
import pl.pa.shopping.list.product.domain.ProductBaseRepositoryInMemory;
import pl.pa.shopping.list.product.domain.ProductBaseService;
import pl.pa.shopping.list.product.error.InvalidProductDataException;
import pl.pa.shopping.list.product.model.ProductDTO;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProductBaseApiTest {

    private static ProductBaseRepository productBaseRepository = new ProductBaseRepositoryInMemory();
    private static ProductBaseService productBaseService = new ProductBaseService(productBaseRepository);

    private static ProductBaseApi productBaseApi = new ProductBaseApi(productBaseService);

    @BeforeAll
    static void init() {
        ProductDTO productDTO = ProductDTO.builder().name("pomidor").
                productAmount(2.0).build();
        productBaseApi.addProduct(productDTO);
    }

    @Test
    @Order(1)
    void shouldAddNewProduct() {
        //given
        ProductDTO newProduct = ProductDTO.builder().name("papryka").
                productAmount(1.0).build();
        productBaseApi.addProduct(newProduct);
        //when
        ProductDTO result = productBaseApi.findProduct("papryka");
        //then
        assertEquals(newProduct.getName(), result.getName());
    }

    @Test
    @Order(2)
    void shouldNotAddIfNewProductExist() {
        //given
        ProductDTO newProduct = ProductDTO.builder().name("pomidor").
                productAmount(2.0).build();

        //then
        assertThrows(InvalidProductDataException.class, () -> {
            productBaseApi.addProduct(newProduct);
        });
    }

    @Test
    @Order(3)
    void shouldFindIfProductExist() {
        //then
        assertDoesNotThrow(() -> productBaseApi.findProduct("pomidor"));
    }

    @Test
    @Order(4)
    void shouldNotFindIfProductNotExist() {
        //then
        assertThrows(InvalidProductDataException.class, () -> {
            productBaseApi.findProduct("sałata");
        });
    }

    @Test
    @Order(5)
    void shouldDeleteIfProductExist() {
        //when
        productBaseApi.deleteProduct("pomidor");
        //then
        assertThrows(InvalidProductDataException.class, () -> {
            productBaseApi.findProduct("pomidor");
        });
    }

    @Test
    @Order(6)
    void shouldNotDeleteIfProductNotExist() {
        //then
        assertThrows(InvalidProductDataException.class, () -> {
            productBaseApi.deleteProduct("burak");
        });
    }

}
