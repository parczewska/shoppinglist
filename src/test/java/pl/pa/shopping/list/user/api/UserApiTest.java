package pl.pa.shopping.list.user.api;

import org.junit.jupiter.api.BeforeAll;
import pl.pa.shopping.list.list.domain.ProductsListRepository;
import pl.pa.shopping.list.list.domain.ProductsListRepositoryInMemory;
import pl.pa.shopping.list.list.domain.ProductsListService;
import pl.pa.shopping.list.list.model.ProductsListDTO;
import pl.pa.shopping.list.product.api.ProductBaseApi;
import pl.pa.shopping.list.product.domain.ProductBaseRepository;
import pl.pa.shopping.list.product.domain.ProductBaseRepositoryInMemory;
import pl.pa.shopping.list.product.domain.ProductBaseService;
import pl.pa.shopping.list.product.model.ProductDTO;
import pl.pa.shopping.list.product.model.ProductGroup;
import pl.pa.shopping.list.user.domain.UserRepository;
import pl.pa.shopping.list.user.domain.UserRepositoryInMemory;
import pl.pa.shopping.list.user.domain.UserService;
import pl.pa.shopping.list.user.model.UserDTO;

import java.util.ArrayList;
import java.util.List;

public class UserApiTest {

    private static UserRepository userRepository = new UserRepositoryInMemory();
    private static UserService userService = new UserService(userRepository);
    private static UserApi userApi = new UserApi(userService);
    private static ProductBaseRepository productBaseRepository = new ProductBaseRepositoryInMemory();
    private static ProductBaseService productBaseService = new ProductBaseService(productBaseRepository);
    private static ProductBaseApi productBaseApi = new ProductBaseApi(productBaseService);
    private static ProductsListRepository productsListRepository = new ProductsListRepositoryInMemory();
    private static ProductsListService productsListService = new ProductsListService(productsListRepository, productBaseRepository);

    @BeforeAll
    static void initProducts() {

        ProductDTO product1 = ProductDTO.builder().name("mleko").
                productAmount(2.0).
                productGroup(ProductGroup.DAIRY).
                calories(70.0).build();
        productBaseApi.addProduct(product1);

        ProductDTO product2 = ProductDTO.builder().name("ryż").
                productAmount(1.0).
                productGroup(ProductGroup.CEREAL_PRODUCTS).
                calories(150.0).build();
        productBaseApi.addProduct(product2);

        ProductDTO product3 = ProductDTO.builder().name("papryka").
                productAmount(3.0).
                productGroup(ProductGroup.VEGETABLES).
                calories(10.0).build();
        productBaseApi.addProduct(product3);

        ProductDTO product4 = ProductDTO.builder().name("cytryna").
                productAmount(3.0).
                productGroup(ProductGroup.FRUITS).
                calories(5.0).build();
        productBaseApi.addProduct(product4);
    }

    @BeforeAll
    static void initProductsList() {

        List<ProductDTO> list1 = new ArrayList<>();


        ProductsListDTO productsList1 = ProductsListDTO.builder().name("śniadanie").products(list1.).build();

    }


    UserDTO newUser = UserDTO.builder().login("anowak").
            name("Anna").
            lastname("Nowak").
            lists().build();
        userApi.addUser(newUser);
}
}
