package pl.pa.shopping.list.user.domain;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import pl.pa.shopping.list.user.model.UserDAO;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class UserRepositoryInMemory implements UserRepository {

    private Set<UserDAO> users = new HashSet<>();

    @Override
    public Optional<UserDAO> findByLogin(String login) {
        return users.stream().filter(e -> e.getLogin().equals(login)).findFirst();
    }

    @Override
    public List<UserDAO> findAll() {
        return users.stream().collect(Collectors.toUnmodifiableList());
    }

    @Override
    public List<UserDAO> findAll(Sort sort) {
        return null;
    }

    @Override
    public Page<UserDAO> findAll(Pageable pageable) {
        return null;
    }

    @Override
    public List<UserDAO> findAllById(Iterable<Long> longs) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(UserDAO entity) {
        users.remove(entity);

    }

    @Override
    public void deleteAllById(Iterable<? extends Long> longs) {

    }

    @Override
    public void deleteAll(Iterable<? extends UserDAO> entities) {

    }

    @Override
    public void deleteAll() {

    }

    @Override
    public <S extends UserDAO> S save(S entity) {
        users.add(entity);
        return entity;
    }

    @Override
    public <S extends UserDAO> List<S> saveAll(Iterable<S> entities) {
        return null;
    }

    @Override
    public Optional<UserDAO> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public void flush() {

    }

    @Override
    public <S extends UserDAO> S saveAndFlush(S entity) {
        return null;
    }

    @Override
    public <S extends UserDAO> List<S> saveAllAndFlush(Iterable<S> entities) {
        return null;
    }

    @Override
    public void deleteAllInBatch(Iterable<UserDAO> entities) {

    }

    @Override
    public void deleteAllByIdInBatch(Iterable<Long> longs) {

    }

    @Override
    public void deleteAllInBatch() {

    }

    @Override
    public UserDAO getOne(Long aLong) {
        return null;
    }

    @Override
    public UserDAO getById(Long aLong) {
        return null;
    }

    @Override
    public UserDAO getReferenceById(Long aLong) {
        return null;
    }

    @Override
    public <S extends UserDAO> Optional<S> findOne(Example<S> example) {
        return Optional.empty();
    }

    @Override
    public <S extends UserDAO> List<S> findAll(Example<S> example) {
        return null;
    }

    @Override
    public <S extends UserDAO> List<S> findAll(Example<S> example, Sort sort) {
        return null;
    }

    @Override
    public <S extends UserDAO> Page<S> findAll(Example<S> example, Pageable pageable) {
        return null;
    }

    @Override
    public <S extends UserDAO> long count(Example<S> example) {
        return 0;
    }

    @Override
    public <S extends UserDAO> boolean exists(Example<S> example) {
        return false;
    }

    @Override
    public <S extends UserDAO, R> R findBy(Example<S> example, Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        return null;
    }
}
